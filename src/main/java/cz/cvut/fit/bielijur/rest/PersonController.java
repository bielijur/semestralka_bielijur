package cz.cvut.fit.bielijur.rest;

import cz.cvut.fit.bielijur.data.model.MealEntity;
import cz.cvut.fit.bielijur.data.model.PlanEntity;
import cz.cvut.fit.bielijur.service.PersonService;
import cz.cvut.fit.bielijur.data.model.PersonEntity;
import cz.cvut.fit.bielijur.service.PlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/person")
public class PersonController {
    @Autowired
    private PersonService personService;

    @Autowired
    private PlanService planService;

    @PostMapping
    public ResponseEntity<PersonEntity> createEntity (@RequestBody PersonEntity ml) {
        if (personService.readById(ml.getNickName()).isPresent())
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        return ResponseEntity.ok(personService.createOrUpdate(ml));
    }

    @GetMapping(value = "/{name}")
    public ResponseEntity<Optional<PersonEntity>> getEntity (@PathVariable String name) {
        if (!personService.readById(name).isPresent()) {
            return ResponseEntity.notFound().build();
        }

        Optional<PersonEntity> personEntity = personService.readById(name);
        for (PlanEntity plan : personEntity.get().getPlans()) {
            plan.setCreator(null);
            for (MealEntity mealEntity : plan.getMeals())
                mealEntity.setPlans(null);
        }

        return ResponseEntity.ok(personEntity);
    }

    @GetMapping
    public ResponseEntity<Collection<PersonEntity>> getAllEntities () {
        Collection<PersonEntity> personEntities = personService.readAll();
        for (PersonEntity personEntity : personEntities)
            for (PlanEntity plan : personEntity.getPlans()) {
                plan.setCreator(null);
                for (MealEntity mealEntity : plan.getMeals())
                    mealEntity.setPlans(null);
            }

        return ResponseEntity.ok(personService.readAll());
    }

    @PutMapping
    public ResponseEntity<String> updateEntity (@RequestBody PersonEntity pr) {
        personService.createOrUpdate(pr);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping(value = "/{personId}/plans/{planId}")
    public ResponseEntity<String> addPlan (@PathVariable String personId, @PathVariable String planId) {
        try {
            personService.addPlanToPerson(personId, planId);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        catch (IllegalArgumentException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping(value = "/{name}")
    public ResponseEntity<String> deleteEntity (@PathVariable String name) {
        try {
            personService.removeById(name);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        catch (IllegalArgumentException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping
    public ResponseEntity<String> deleteAllEntities () {
        try {
            personService.removeAll();
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @DeleteMapping(value = "/{personId}/plans/{planId}")
    public ResponseEntity<String> deletePlanFromEntity (@PathVariable String personId, @PathVariable String planId ) {
        try {
            personService.removePlanFromPerson(personId, planId);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        catch (IllegalArgumentException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
