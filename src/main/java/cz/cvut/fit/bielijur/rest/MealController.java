package cz.cvut.fit.bielijur.rest;

import cz.cvut.fit.bielijur.data.model.PersonEntity;
import cz.cvut.fit.bielijur.data.model.PlanEntity;
import cz.cvut.fit.bielijur.service.MealService;
import cz.cvut.fit.bielijur.data.model.MealEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Optional;


@RestController
@RequestMapping(value = "/meal")
public class MealController {
    @Autowired
    private MealService mealService;

    @PostMapping
    public ResponseEntity<MealEntity> createEntity(@RequestBody MealEntity ml) {
        if (mealService.readById(ml.getName()).isPresent())
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        return ResponseEntity.ok(mealService.createOrUpdate(ml));
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Optional<MealEntity>> getEntity(@PathVariable String id) {
        if (!mealService.readById(id).isPresent()) {
            return ResponseEntity.notFound().build();
        }

        Optional<MealEntity> mealEntity = mealService.readById(id);
        Collection<PlanEntity> plans = mealEntity.get().getPlans();
        for (PlanEntity plan : plans) {
            plan.setMeals(null);
            if (plan.getCreator() != null)
                plan.getCreator().setPlans(null);

        }

        return ResponseEntity.ok(mealEntity);
    }

    @GetMapping
    public Collection<MealEntity> getAllEntities() {
        Collection<MealEntity> mealEntities = mealService.readAll();
        for (MealEntity mealEntity : mealEntities) {
            for (PlanEntity plan : mealEntity.getPlans()) {
                plan.setMeals(null);
                if (plan.getCreator() != null)
                    plan.getCreator().setPlans(null);
            }
        }

        return mealEntities;
    }

    @PutMapping
    public ResponseEntity<MealEntity> updateEntity(@RequestBody MealEntity ml) {
        return ResponseEntity.ok(mealService.createOrUpdate(ml));
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<String> deleteEntity(@PathVariable String id) {
        try {
            mealService.removeById(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping
    public ResponseEntity<String> deleteAllEntities() {
        try {
            mealService.removeAll();
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
}
