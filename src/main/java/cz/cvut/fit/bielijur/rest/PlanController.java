package cz.cvut.fit.bielijur.rest;

import cz.cvut.fit.bielijur.data.model.MealEntity;
import cz.cvut.fit.bielijur.data.model.PersonEntity;
import cz.cvut.fit.bielijur.service.MealService;
import cz.cvut.fit.bielijur.service.PlanService;
import cz.cvut.fit.bielijur.data.model.PlanEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Optional;

@RestController
@RequestMapping(value = "/plan")
public class PlanController {
    @Autowired
    private PlanService planService;

    @PostMapping(value = "/{name}")
    public ResponseEntity<PlanEntity> createEntity (@PathVariable String name) {
        if (planService.readById(name).isPresent())
            return ResponseEntity.status(HttpStatus.CONFLICT).build();

        PlanEntity planEntity = new PlanEntity(name);

        return ResponseEntity.ok(planService.createOrUpdate(planEntity));
    }

    @GetMapping(value = "/{name}")
    public ResponseEntity<Optional<PlanEntity>> getEntity (@PathVariable String name) {
        if (!planService.readById(name).isPresent()) {
            return ResponseEntity.notFound().build();
        }

        Optional<PlanEntity> planEntity = planService.readById(name);
        if (planEntity.get().getCreator() != null)
            planEntity.get().getCreator().setPlans(null);
        for (MealEntity mealEntity : planEntity.get().getMeals())
            mealEntity.setPlans(null);

        return ResponseEntity.ok(planEntity);
    }

    @GetMapping
    public ResponseEntity<Collection<PlanEntity>> getAllEntities () {
        Collection<PlanEntity> planEntities = planService.readAll();
        for (PlanEntity planEntity : planEntities) {
            if (planEntity.getCreator() != null)
                planEntity.getCreator().setPlans(null);
            for (MealEntity mealEntity : planEntity.getMeals())
                mealEntity.setPlans(null);
        }
        return ResponseEntity.ok(planService.readAll());
    }

    @PutMapping
    public ResponseEntity<String> updateEntity (@RequestBody PlanEntity pr) {
        planService.createOrUpdate(pr);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping(value = "/{planId}/meals/{mealId}")
    public ResponseEntity<String> addMeal (@PathVariable String planId, @PathVariable String mealId) {
        try {
            planService.addMealToPlan(planId, mealId);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        catch (IllegalArgumentException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping(value = "/{name}")
    public ResponseEntity<String> deleteEntity (@PathVariable String name) {
        try {
            planService.removeById(name);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        catch (IllegalArgumentException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping
    public ResponseEntity<String> deleteAllEntities () {
        try {
            planService.removeAll();
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @DeleteMapping(value = "/{planId}/creator/{personId}")
    public ResponseEntity<String> deletePersonFromEntity (@PathVariable String planId, @PathVariable String personId ) {
        try {
            planService.removePersonFromPlan(planId, personId);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        catch (IllegalArgumentException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping(value = "/{planId}/meals/{mealId}")
    public ResponseEntity<String> deleteMealFromEntity (@PathVariable String planId, @PathVariable String mealId) {
        try {
            planService.removeMealFromPlan(planId, mealId);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        catch (IllegalArgumentException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}

