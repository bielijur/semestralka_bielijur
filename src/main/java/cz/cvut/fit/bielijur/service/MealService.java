package cz.cvut.fit.bielijur.service;

import cz.cvut.fit.bielijur.data.dao.PlanRepository;
import cz.cvut.fit.bielijur.data.dao.MealRepository;
import cz.cvut.fit.bielijur.data.model.MealEntity;
import cz.cvut.fit.bielijur.data.model.PersonEntity;
import cz.cvut.fit.bielijur.data.model.PlanEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.Collection;
import java.util.Optional;

@Service
public class MealService {
    @Autowired
    private MealRepository repository;

    @Autowired
    private PlanRepository planRepository;

    @Transactional
    public MealEntity createOrUpdate(MealEntity e) {
        if (repository.findById(e.getName()).isPresent()) {
            MealEntity mealEntity = repository.findById(e.getName()).get();
            Integer caloriesDiff = e.getCalories() - mealEntity.getCalories();
            Integer carbsDiff = e.getCarbs() - mealEntity.getCarbs();
            Integer fatsDiff = e.getFats() - mealEntity.getFats();
            Integer proteinsDiff = e.getProtein() - mealEntity.getProtein();

            Collection<PlanEntity> planEntities = mealEntity.getPlans();
            for (PlanEntity planEntity : planEntities) {
                planEntity.setTotalCalories(planEntity.getTotalCalories() + caloriesDiff);
                planEntity.setTotalCarbs(planEntity.getTotalCarbs() + carbsDiff);
                planEntity.setTotalFats(planEntity.getTotalFats() + fatsDiff);
                planEntity.setTotalProteins(planEntity.getTotalProteins() + proteinsDiff);
                planRepository.save(planEntity);
            }
        }
        return repository.save(e);
    }

    @Transactional(readOnly = true)
    public Collection<MealEntity> readAll() {
        return repository.findAll();
    }

    @Transactional(readOnly = true)
    public Optional<MealEntity> readById(String name) {
        return repository.findById(name);
    }

    @Transactional
    public void removeById (String name) {
        if (!repository.findById(name).isPresent())
            throw new IllegalArgumentException();

        // most likely treba pri takomto type deletu odobrat z totalCalories atd.
        MealEntity mealEntity = repository.findById(name).get();

        for (PlanEntity planEntity : mealEntity.getPlans()) {
            planEntity.getMeals().remove(mealEntity);
            planEntity.setTotalCalories(planEntity.getTotalCalories() - mealEntity.getCalories());
            planEntity.setTotalCarbs(planEntity.getTotalCarbs() - mealEntity.getCarbs());
            planEntity.setTotalFats(planEntity.getTotalFats() - mealEntity.getFats());
            planEntity.setTotalProteins(planEntity.getTotalProteins() - mealEntity.getProtein());

            planRepository.save(planEntity);
        }
        repository.deleteById(name);
    }

    @Transactional
    public void removeAll () {
        // the same as commented above
        for (MealEntity mealEntity : repository.findAll()) {
            for (PlanEntity planEntity : mealEntity.getPlans()) {
                planEntity.getMeals().remove(mealEntity);
                planEntity.setTotalCalories(planEntity.getTotalCalories() - mealEntity.getCalories());
                planEntity.setTotalCarbs(planEntity.getTotalCarbs() - mealEntity.getCarbs());
                planEntity.setTotalFats(planEntity.getTotalFats() - mealEntity.getFats());
                planEntity.setTotalProteins(planEntity.getTotalProteins() - mealEntity.getProtein());
                planRepository.save(planEntity);
            }
        }
        repository.deleteAll();
    }
}
