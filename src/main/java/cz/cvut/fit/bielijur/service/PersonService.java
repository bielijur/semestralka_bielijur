package cz.cvut.fit.bielijur.service;

import cz.cvut.fit.bielijur.data.dao.PersonRepository;
import cz.cvut.fit.bielijur.data.dao.PlanRepository;
import cz.cvut.fit.bielijur.data.model.MealEntity;
import cz.cvut.fit.bielijur.data.model.PersonEntity;
import cz.cvut.fit.bielijur.data.model.PlanEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

@Service
public class PersonService {
    @Autowired
    private PersonRepository repository;

    @Autowired
    private PlanRepository planRepository;

    @Transactional
    public PersonEntity createOrUpdate(PersonEntity e) {
        if (repository.findById(e.getNickName()).isPresent()) {
            Collection<PlanEntity> planEntities = repository.findById(e.getNickName()).get().getPlans();
            e.setPlans(planEntities);
        }
        return repository.save(e);
    }

    @Transactional(readOnly = true)
    public Collection<PersonEntity> readAll() {

        return repository.findAll();
    }

    @Transactional(readOnly = true)
    public Optional<PersonEntity> readById(String name) {
        return repository.findById(name);
    }

    @Transactional
    public void removeById (String name) {
        if (!repository.findById(name).isPresent())
            throw new IllegalArgumentException();

        Collection<PlanEntity> planEntities = repository.findById(name).get().getPlans();

        for (PlanEntity planEntity : planEntities) {
            planEntity.setCreator(null);
            planRepository.save(planEntity);
        }
        repository.deleteById(name);
    }

    @Transactional
    public void removeAll () {
        Collection<PlanEntity> planEntities = planRepository.findAll();
        for (PlanEntity planEntity : planEntities) {
            planEntity.setCreator(null);
        }

        repository.deleteAll();
    }

    @Transactional
    public void addPlanToPerson (String personId, String planId) {
        if (!repository.findById(personId).isPresent() || !planRepository.findById(planId).isPresent())
            throw new IllegalArgumentException();
//        if (planRepository.findById(planId).get().getCreator().getNickName() == null || planRepository.findById(planId).get().getCreator().getNickName().equals(repository.findById(personId).get().getNickName()))
//            return;
        PersonEntity personEntity = repository.findById(personId).get();
        PlanEntity planEntity = planRepository.findById(planId).get();

        personEntity.getPlans().add(planEntity);
        repository.save(personEntity);
        planEntity.setCreator(personEntity);
        planRepository.save(planEntity);
    }

    @Transactional
    public void removePlanFromPerson (String personId, String planId) {
        if (!repository.findById(personId).isPresent() || !planRepository.findById(planId).isPresent())
            throw new IllegalArgumentException();

        PersonEntity personEntity = repository.findById(personId).get();
        PlanEntity planEntity = planRepository.findById(planId).get();

        personEntity.getPlans().remove(planEntity);
        repository.save(personEntity);
        planEntity.setCreator(null);
        planRepository.save(planEntity);
    }
}
