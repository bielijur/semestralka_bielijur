package cz.cvut.fit.bielijur.service;

import cz.cvut.fit.bielijur.data.dao.PersonRepository;
import cz.cvut.fit.bielijur.data.dao.PlanRepository;
import cz.cvut.fit.bielijur.data.dao.MealRepository;
import cz.cvut.fit.bielijur.data.model.MealEntity;
import cz.cvut.fit.bielijur.data.model.PersonEntity;
import cz.cvut.fit.bielijur.data.model.PlanEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

@Service
public class PlanService {
    @Autowired
    private PlanRepository repository;

    @Autowired
    private MealRepository mealRepository;

    @Autowired
    private PersonRepository personRepository;

    @Transactional
    public PlanEntity createOrUpdate(PlanEntity e) {
        if (repository.findById(e.getName()).isPresent()) {
            PersonEntity personEntity = repository.findById(e.getName()).get().getCreator();
            Collection<MealEntity> mealEntities = repository.findById(e.getName()).get().getMeals();
            e.setMeals(mealEntities);
            e.setCreator(personEntity);
        }
        return repository.save(e);
    }

    @Transactional(readOnly = true)
    public Collection<PlanEntity> readAll() {

        return repository.findAll();
    }

    @Transactional(readOnly = true)
    public Optional<PlanEntity> readById(String name) {
        return repository.findById(name);
    }

    @Transactional
    public void removeById (String name) {
        if (!repository.findById(name).isPresent())
            throw new IllegalArgumentException();

        PlanEntity planEntity = repository.findById(name).get();
        PersonEntity personEntity = repository.findById(name).get().getCreator();
        personEntity.getPlans().remove(planEntity);
        personRepository.save(personEntity);
////        for (MealEntity mealEntity : planEntity.getMeals()) {
////            mealEntity.getPlans().remove(planEntity);
////            mealRepository.save(mealEntity);
////        }
        repository.deleteById(name);
    }

    @Transactional
    public void removeAll () {
        Collection<PlanEntity> planEntities = repository.findAll();
        for (PlanEntity planEntity : planEntities) {
            if (planEntity.getCreator() != null) {
                PersonEntity personEntity = planEntity.getCreator();
                personEntity.setPlans(null);
                personRepository.save(personEntity);
            }
            for (MealEntity mealEntity : planEntity.getMeals()) {
                mealEntity.setPlans(null);
                mealRepository.save(mealEntity);
            }

        }

        repository.deleteAll();
    }

//    @Transactional
//    public void addPersonToPlan (String planId, String personId) {
//        if (!repository.findById(planId).isPresent() || !personRepository.findById(personId).isPresent())
//            throw new IllegalArgumentException();
//
//        PlanEntity planEntity = repository.findById(planId).get();
//        PersonEntity personEntity = personRepository.findById(personId).get();
//        if (planEntity.getCreator().getNickName().equals(personEntity.getNickName()))
//            return;
//
//        planEntity.setCreator(personEntity);
//        repository.save(planEntity);
////        personEntity.getPlans().add(planEntity);
////        personRepository.save(personEntity);
//    }

    @Transactional
    public void addMealToPlan (String planId, String mealId) {
        if (!repository.findById(planId).isPresent() || !mealRepository.findById(mealId).isPresent())
            throw new IllegalArgumentException();


        PlanEntity planEntity = repository.findById(planId).get();
        MealEntity mealEntity = mealRepository.findById(mealId).get();
        if (planEntity.getMeals().contains(mealEntity))
            return;

        planEntity.setTotalCalories(planEntity.getTotalCalories() + mealEntity.getCalories());
        planEntity.setTotalCarbs(planEntity.getTotalCarbs() + mealEntity.getCarbs());
        planEntity.setTotalFats(planEntity.getTotalFats() + mealEntity.getFats());
        planEntity.setTotalProteins(planEntity.getTotalProteins() + mealEntity.getProtein());
        planEntity.getMeals().add(mealRepository.findById(mealId).get());
        repository.save(planEntity);
    }

    @Transactional
    public void removePersonFromPlan (String planId, String personId) {
        if (!repository.findById(planId).isPresent() || !personRepository.findById(personId).isPresent())
            throw new IllegalArgumentException();

        PlanEntity planEntity = repository.findById(planId).get();
        PersonEntity personEntity = personRepository.findById(personId).get();

        personEntity.getPlans().remove(planEntity);
        personRepository.save(personEntity);
        planEntity.setCreator(null);
        repository.save(planEntity);
    }

    @Transactional
    public void removeMealFromPlan (String planId, String mealId) {
        if (!repository.findById(planId).isPresent() || !mealRepository.findById(mealId).isPresent())
            throw new IllegalArgumentException();

        PlanEntity planEntity = repository.findById(planId).get();
        MealEntity mealEntity = mealRepository.findById(mealId).get();
        planEntity.setTotalCalories(planEntity.getTotalCalories() - mealEntity.getCalories());
        planEntity.setTotalCarbs(planEntity.getTotalCarbs() - mealEntity.getCarbs());
        planEntity.setTotalFats(planEntity.getTotalFats() - mealEntity.getFats());
        planEntity.setTotalProteins(planEntity.getTotalProteins() - mealEntity.getProtein());

        planEntity.getMeals().remove(mealEntity);
        repository.save(planEntity);
    }
}
