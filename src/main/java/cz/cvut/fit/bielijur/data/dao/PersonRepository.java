package cz.cvut.fit.bielijur.data.dao;

import cz.cvut.fit.bielijur.data.model.PersonEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends JpaRepository<PersonEntity, String> {
}
