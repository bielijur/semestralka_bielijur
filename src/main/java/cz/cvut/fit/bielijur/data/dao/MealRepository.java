package cz.cvut.fit.bielijur.data.dao;

import cz.cvut.fit.bielijur.data.model.MealEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MealRepository extends JpaRepository<MealEntity, String> {
}
