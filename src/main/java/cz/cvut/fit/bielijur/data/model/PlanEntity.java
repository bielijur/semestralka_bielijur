package cz.cvut.fit.bielijur.data.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import java.util.Collection;

@Entity
public class PlanEntity {

    @Id
    private String name;

    private Integer totalCalories;
    private Integer totalFats;
    private Integer totalCarbs;
    private Integer totalProteins;

    @ManyToMany
    Collection<MealEntity> meals;

    @ManyToOne
    PersonEntity creator;

    public PlanEntity() {
    }

    public PlanEntity(String name) {
        this.name = name;
        totalCalories = 0;
        totalFats = 0;
        totalCarbs = 0;
        totalProteins = 0;
    }

    public PlanEntity(String name, Integer totalCalories, Integer totalFats, Integer totalCarbs, Integer totalProteins) {
        this.name = name;
        this.totalCalories = totalCalories;
        this.totalFats = totalFats;
        this.totalCarbs = totalCarbs;
        this.totalProteins = totalProteins;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getTotalCalories() {
        return totalCalories;
    }

    public void setTotalCalories(Integer totalCalories) {
        this.totalCalories = totalCalories;
    }

    public Integer getTotalFats() {
        return totalFats;
    }

    public void setTotalFats(Integer totalFats) {
        this.totalFats = totalFats;
    }

    public Integer getTotalCarbs() {
        return totalCarbs;
    }

    public void setTotalCarbs(Integer totalCarbs) {
        this.totalCarbs = totalCarbs;
    }

    public Integer getTotalProteins() {
        return totalProteins;
    }

    public void setTotalProteins(Integer totalProteins) {
        this.totalProteins = totalProteins;
    }

    public Collection<MealEntity> getMeals() {
        return meals;
    }

    public void setMeals(Collection<MealEntity> meals) {
        this.meals = meals;
    }

    public PersonEntity getCreator() {
        return creator;
    }

    public void setCreator(PersonEntity creator) {
        this.creator = creator;
    }
}
