package cz.cvut.fit.bielijur.data.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.Collection;


@Entity
public class PersonEntity {

        @Id
        private String nickName;

        private String firstName;
        private String lastName;
        private Integer caloriesBurntDaily;

        @OneToMany
        Collection<PlanEntity> plans;

    public PersonEntity() {
    }

    public PersonEntity(String nickName, String firstName, String lastName, Integer caloriesBurntDaily) {
        this.nickName = nickName;
        this.firstName = firstName;
        this.lastName = lastName;
        this.caloriesBurntDaily = caloriesBurntDaily;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getCaloriesBurntDaily() {
        return caloriesBurntDaily;
    }

    public void setCaloriesBurntDaily(Integer caloriesBurntDaily) {
        this.caloriesBurntDaily = caloriesBurntDaily;
    }

    public Collection<PlanEntity> getPlans() {
        return plans;
    }

    public void setPlans(Collection<PlanEntity> plans) {
        this.plans = plans;
    }
}
