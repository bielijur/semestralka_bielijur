package cz.cvut.fit.bielijur.data.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.Collection;

@Entity
public class MealEntity {

    @Id
    private String name;

    private Integer calories;
    private Integer fats;
    private Integer carbs;
    private Integer protein;

    @ManyToMany(mappedBy = "meals")
    Collection<PlanEntity> plans;

    public MealEntity(String name, Integer totalCalories, Integer totalFats, Integer totalCarbs, Integer totalProtein) {
        this.name = name;
        this.calories = totalCalories;
        this.fats = totalFats;
        this.carbs = totalCarbs;
        this.protein = totalProtein;
    }

    public MealEntity() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCalories() {
        return calories;
    }

    public void setCalories(Integer calories) {
        this.calories = calories;
    }

    public Integer getFats() {
        return fats;
    }

    public void setFats(Integer fats) {
        this.fats = fats;
    }

    public Integer getCarbs() {
        return carbs;
    }

    public void setCarbs(Integer carbs) {
        this.carbs = carbs;
    }

    public Integer getProtein() {
        return protein;
    }

    public void setProtein(Integer protein) {
        this.protein = protein;
    }

    public Collection<PlanEntity> getPlans() {
        return plans;
    }

    public void setPlans(Collection<PlanEntity> plans) {
        this.plans = plans;
    }
}
