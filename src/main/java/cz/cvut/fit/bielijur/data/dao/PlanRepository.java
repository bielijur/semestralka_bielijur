package cz.cvut.fit.bielijur.data.dao;

import cz.cvut.fit.bielijur.data.model.PlanEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlanRepository extends JpaRepository<PlanEntity, String> {
}
